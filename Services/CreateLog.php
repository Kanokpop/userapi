<?php
namespace Services;

use App\Models\log;
use Carbon\Carbon;

class CreateLog
{
    public static function create($log,$response)
    {
        $_log = new log();
        $_log->request = $log;
        // $_log->action = $action;
        $_log->response = $response;
        $_log->save();
        return $_log;
    }
    public static function update($objectID,$action)
    {
        $_log = log::find($objectID);
        $_log->action = $action;
        $_log->updated_at =  Carbon::now()->timestamp ;
        $_log->save();
        return $_log;
    }
}
