@ECHO OFF
setx DOCKER_IMG_TAG %2
call refreshenv
docker-compose build %1
docker-compose push %1
setx DOCKER_IMG_TAG "latest"
call refreshenv
docker-compose build %1
docker-compose push %1