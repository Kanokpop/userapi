<?php

namespace App\Services\Blogs;

use \Illuminate\Support\Facades\Facade;

class CreateFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'CreateService';
    }
}
