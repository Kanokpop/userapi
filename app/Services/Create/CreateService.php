<?php

namespace App\Services\create;

use App\Repositories\Create\CreateUserRepositoryInterface;

class CreateService
{
    var $service;

    public function __construct(CreateUserRepositoryInterface $service)
    {
        $this->service = $service;
    }

    public function create($param)
    {
        return $this->service->create($param);
    }
}
