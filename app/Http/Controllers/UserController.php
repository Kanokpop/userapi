<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Create\CreateUserRepositoryInterface;
use App\Repositories\Read\GetUserRepositoryInterface;
use App\Repositories\Update\UpdateUserRepositoryInterface;
use App\Repositories\Delete\DeleteUserRepositoryInterface;
use Services\CreateLog;


class UserController extends Controller
{
    var $serviceCreate, $serviceRead, $serviceUpdate, $serviceDelete;

    public function __construct(
        CreateUserRepositoryInterface $serviceCreate,
        GetUserRepositoryInterface $serviceRead,
        UpdateUserRepositoryInterface $serviceUpdate,
        DeleteUserRepositoryInterface $serviceDelete
    ) {
        $this->serviceCreate = $serviceCreate;
        $this->serviceRead = $serviceRead;
        $this->serviceUpdate = $serviceUpdate;
        $this->serviceDelete = $serviceDelete;
    }


    public function checkInput(Request $request)
    {
        if (count($request->all()) == 7) {
            foreach ($request->all() as $requests) {
                if ($requests == "") {
                    $msg = array('data' => [["Code" => 200, "msg" => 'Input have null']]);
                    CreateLog::create($request->all(), 'create', $msg);
                    return exit(json_encode($msg));
                }
            }
        } else {
            $msg = array('data' => [["Code" => 200, "msg" => 'data incomplete']]);
            CreateLog::create($request->all(), 'create', $msg);
            return exit(json_encode($msg));
        }
    }

    public function Create(Request $request)
    {
        $this->checkInput($request);
        $code = $this->serviceCreate->create($request);
        $_code = array('data' => [$code]);
        return response()->json($_code, 201);
    }

    public function get(Request $request)
    {
        return $this->serviceRead->get($request);
    }

    public function getOne(Request $request)
    {
        return $this->serviceRead->getOne($request);
    }

    public function Delete(Request $request)
    {
        $code = $this->serviceDelete->Delete($request);
        return response()->json($code, 201);
    }

    public function Update(Request $request)
    {
        $this->checkInput($request);
        $code = $this->serviceUpdate->update($request);
        $_code = array('data' => [$code]);
        return response()->json($_code, 201);
    }
}
