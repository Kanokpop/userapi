<?php
namespace App\Http\Middleware;

// use Illuminate\Http\Request;
use Closure;
use Services\CreateLog;
use App\Http\Controllers\UserController;

class CheckCreateLoger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (($request->route('id') == '')&&($request->method() == 'GET')){
            $code = array("Code" => 201, "msg" => "Created");
            $_log = CreateLog::create($request->all(), $code);
            $objectID = $_log['_id'];
            $request['id'] = $objectID;
            return $next($request);
        } else if (($request->method() == 'POST' || "PUT") || "GET" || "delete") {
            $code = array("Code" => 201, "msg" => "Created");
            $_log = CreateLog::create($request->all(), $code);
            $objectID = $_log['_id'];
            $request['id'] = $objectID;
            return $next($request);
        }
    }
}
