<?php
namespace App\Repositories\Update;

// use Illuminate\Http\Request;
use App\Models\user;
use Services\CreateLog;


class UpdateUserRepository implements UpdateUserRepositoryInterface
{
    public function __construct()
    { }

    public function Update($request)
    {
        $code = array("Code" => 201, "msg" => "Updated");
        $user = user::find($request->route('id'));
        if ($user == '') {
            $code = array("Code" => 200, "msg" => 'Data Mismatch');
            return $code;
        } else {
            $user->firstName = $request['firstName'];
            $user->lastName = $request['lastName'];
            $user->age = $request['age'];
            $user->sex = $request['sex'];
            $user->email = $request['email'];
            $user->phone = $request['phone'];
            $user->save();
            CreateLog::update($request['id'],'Updated');
            return $code;
        }
    }
}
