<?php

namespace App\Repositories\Update;


interface UpdateUserRepositoryInterface
{

    function update($request);

}
