<?php
namespace App\Repositories\Delete;

// use Illuminate\Http\Request;
use App\Models\user;
use Services\CreateLog;


class DeleteUserRepository implements DeleteUserRepositoryInterface
{
    public function __construct()
    { }

    public function Delete($id)
    {
        $user = user::find($id->route('id'));
        if ($user == '') {
            return array('data' => [["Code" => 200, "msg" => 'Data Mismatch']]);
        } else {
            $user->delete();
            CreateLog::update($id['id'], 'Deleted');
            return array('data' => [["Code" => 200, "msg" => 'Deleted']]);
        }
    }
}
