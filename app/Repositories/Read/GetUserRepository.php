<?php
namespace App\Repositories\Read;

use App\Models\user;
use Services\CreateLog;


class GetUserRepository implements GetUserRepositoryInterface
{
    public function __construct()
    { }

    public function get($logId)
    {
        $user = user::get();
        CreateLog::update($logId['id'], 'Get');
        return $user;
    }
    public function getOne($id)
    {
        $user = user::find($id->route('id'));
        if ($user == '') {
            return array('data' => [["Code" => 200, "msg" => 'Data Mismatch']]);
        } else {
            CreateLog::update($id['id'], 'GetOne');
            return $user;
        }
    }
}
