<?php

namespace App\Repositories\Read;


interface GetUserRepositoryInterface
{

    function get($logId);
    function getOne($id);

}
