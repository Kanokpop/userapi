<?php
namespace App\Repositories\Create;

// use Illuminate\Http\Request;
use App\Models\user;
use Services\CreateLog;


class CreateUserRepository implements CreateUserRepositoryInterface
{
    public function __construct()
    { }

    public function create($input)
    {
        $user = new user();
        $user->firstName = $input['firstName'];
        $user->lastName = $input['lastName'];
        $user->age = $input['age'];
        $user->sex = $input['sex'];
        $user->email = $input['email'];
        $user->phone = $input['phone'];
        $user->save();

        CreateLog::update($input['id'],'create');

        return array("Code"=>201,"msg"=>"Created");;
    }
}
