<?php

namespace App\Repositories\Create;


interface CreateUserRepositoryInterface
{

    function create($param);

}
