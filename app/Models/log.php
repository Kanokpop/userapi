<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class log extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'logs';
    protected $fillable = [
        'request','action','response'
    ];
}
