<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class user extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'users';
    protected $fillable = [
        'firstName','lastName','age','sex','email','tel'
    ];
}
