<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Create\CreateUserRepositoryInterface','App\Repositories\Create\CreateUserRepository'
        );
        $this->app->bind(
            'App\Repositories\Update\UpdateUserRepositoryInterface','App\Repositories\Update\UpdateUserRepository'
        );
        $this->app->bind(
            'App\Repositories\Delete\DeleteUserRepositoryInterface','App\Repositories\Delete\DeleteUserRepository'
        );
        $this->app->bind(
            'App\Repositories\Read\GetUserRepositoryInterface','App\Repositories\Read\GetUserRepository'
        );
        

        $this->app->singleton(
            'CreateService',
            function ($app) {
                return new \App\Repositories\Create\CreateUserRepository(
                    $app->make('App\Repositories\Create\CreateUserRepositoryInterface')
                );
            }
        );
        $this->app->singleton(
            'UpdateService',
            function ($app) {
                return new \App\Repositories\Update\UpdateUserRepository(
                    $app->make('App\Repositories\Update\UpdateUserRepositoryInterface')
                );
            }
        );
        $this->app->singleton(
            'UpdateService',
            function ($app) {
                return new \App\Repositories\Delete\DeleteUserRepository(
                    $app->make('App\Repositories\Delete\DeleteUserRepositoryInterface')
                );
            }
        );
        $this->app->singleton(
            'UpdateService',
            function ($app) {
                return new \App\Repositories\Read\GetUserRepository(
                    $app->make('App\Repositories\Read\GetUserRepositoryInterface')
                );
            }
        );
    }

}
