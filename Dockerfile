FROM harbor.touch-ics.com/library/php-apache:7.1

#RUN rm -rf /var/www/html
RUN mkdir /var/project
WORKDIR /var/project
COPY ./ /var/project
#RUN ln -s /var/project/public /var/www/html
RUN rm -r /var/www/html; \
    ln -sf /var/project/public /var/www/html;
RUN composer install
RUN chmod -R 775 storage
RUN chmod -R 775 public
RUN chmod -R 775 bootstrap
RUN chown -R www-data.www-data storage
RUN chown -R www-data.www-data public
RUN chown -R www-data.www-data bootstrap