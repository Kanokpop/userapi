<?php



// $router->resource('/api/user', 'UserController');
// $router->post(['middleware' => 'CheckCreateLoger'],  function () {
//     'UserController@Create';
// });

Route::group(['middleware' => 'chkloger'], function () {
    Route::post('/users', [
        'name' => 'users',
        'uses' => 'UserController@Create'
    ]);
    Route::put('/users/{id}', [
        'name' => 'users',
        'uses' => 'UserController@Update'
    ]);
    Route::get('/users', [
        'name' => 'users',
        'uses' => 'UserController@get'
    ]);
    Route::get('/users/{id}', [
        'name' => 'users',
        'uses' => 'UserController@getOne'
    ]);
    Route::delete('/users/{id}', [
        'name' => 'users',
        'uses' => 'UserController@Delete'
    ]);
});
// $router->post('user/add', ['middleware' => $m1, 'uses' => 'UserController@registerUser' ]);




// $router->post('/api/user', [
//     'name' => 'api.user',
//     'uses' => 'UserController@Create',
//     'middleware' => 'chkloger'
// ]);

// Route::put('/api/user/{id}', [
//     'name' => 'api.user',
//     'uses' => 'UserController@Update'
// ]);



Route::get('/log', 'logController@get');
